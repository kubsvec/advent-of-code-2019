puzzle_input = range(264793, 803936)

def check_doubles(mynumber):
    mynumber = str(mynumber)
    a = mynumber[0]
    b = mynumber[1]
    c = mynumber[2]
    d = mynumber[3]
    e = mynumber[4]
    f = mynumber[5]
    isvalid = False
    if a == b or b == c or c == d or d == e or e == f:
        isvalid = True
    return isvalid

def check_decrease(mynumber):
    mynumber = str(mynumber)
    a = mynumber[0]
    b = mynumber[1]
    c = mynumber[2]
    d = mynumber[3]
    e = mynumber[4]
    f = mynumber[5]
    isvalid = False
    if a <= b and b <= c and c <= d and d <= e and e <= f:
        isvalid = True
    return isvalid

def find_combinations(myrange):
    combinations = 0
    for number in myrange:
        if check_doubles(number) and check_decrease(number):
            combinations += 1
    return combinations

#for the second star

def check_double(mynumber):
    mynumber = str(mynumber)
    a = mynumber[0]
    b = mynumber[1]
    c = mynumber[2]
    d = mynumber[3]
    e = mynumber[4]
    f = mynumber[5]
    isvalid = False
    if (a == b and b != c) or (b == c and c != d and b != a) or (c == d and d != e and c != b) or (d == e and e != f and d != c) or (e == f and e != d):
        isvalid = True
    return isvalid

def find_combinations2(myrange):
    combinations = 0
    for number in myrange:
        if check_double(number) and check_decrease(number):
            combinations += 1
    return combinations
    
