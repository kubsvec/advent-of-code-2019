mass = open(r"C:\Python34\advent-of-code-2019\Day1 - Rocket equation\mass.txt", "r")

#star one - count fuel from a list of mass
def countfuel(myfile):
    values = mass.readlines()
##    print(values)
    total = 0
    for value in values:
        number = value.rstrip("\n")
        number = int(number)
        result = int(number / 3) - 2
        total += result
    return total

def recount_fuel(mynumber):
    newnumber = max(0, (int(mynumber / 3) - 2))
    return newnumber

#star two - every amount of fuel is also a mass by itself, that needs more fuel...
def countfuel_final(myfile):
    values = mass.readlines()
    print(values)
    total = 0
    for value in values:
        print("\nCounting for value", value)
        adding = 0
        number = value.rstrip("\n")
        number = int(number)
        result = recount_fuel(number)
        adding += result
        print("Adding", adding, "as the base value for mass")
        #for any number bigger than 9 we need to rerun the formula, until it is smaller than 9,
        #while adding all values bigger than 0 to the result
        while result >= 9:
            result = recount_fuel(result)
            adding += result
            print("Adding", result, "as an addition for fuel")
        total += adding
        print("In total we added", adding, "for both the mass and the fuel. The total is now", total, "\n")
    print("RESULT:", total)

countfuel_final(mass)
