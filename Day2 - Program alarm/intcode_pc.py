#opcode "1" takes the first two indices after itself, add the corresponding values together and overwrites the value at the
#index given by the third value (1,10,20,30 -> add together values from indices 10 and 20 and overwrite the value at index
#30 with the result)

#opcode "2" takes the first two indices after itself, multiplies the corresponding values and overwrites the value at the
#index given by the third value (1,10,20,30 -> multiply values from indices 10 and 20 and overwrite the value at index
#30 with the result)

def get_codelist(myfile):
    codelist = []
    line = myfile.readline()
    line = line.split(",")
    for number in line:
        codelist.append(int(number))
##    print(codelist)
    return codelist

def run_code(mylist):
    index = 0
    reached_end = False
    while reached_end == False:
        if mylist[index] == 1:
            first = mylist[index + 1]
            second = mylist[index + 2]
            result = (mylist[first]) + (mylist[second])
            position = mylist[index + 3]
            mylist[position] = result
        elif mylist[index] == 2:
            first = mylist[index + 1]
            second = mylist[index + 2]
            result = (mylist[first]) * (mylist[second])
            position = mylist[index + 3]
            mylist[position] = result
        elif mylist[index] == 99:
            reached_end = True
        index += 4
##    print(mylist)

def run_programm(noun, verb):
    myfile = open(r"C:\Python34\advent-of-code-2019\Day2 - Program alarm\code.txt", "r")
    base_list = get_codelist(myfile)
    base_list[1] = noun
    base_list[2] = verb
    run_code(base_list)
    #the variable base_list now refers to the NEW list
##    print("The value at address 0 is:", base_list[0])
    return base_list[0]

def find_solution(noun_range, verb_range):
    #input are possible values for nouns and verbs in the form of a range
    #we are looking for a result == 1960720
    noun = noun_range
    verb = verb_range
    for option in noun:
        for variant in verb:
            result = run_programm(option, variant)
            if result == 19690720:
                print("\nFound the result! Noun is ", option, "and verb is", variant, "\n")
                found_result = True
    print("Good job m8.")

find_solution(range(100), range(100))
