wires = open(r"C:\Python34\advent-of-code-2019\Day3 - Wires\wires.txt", "r")
grid_size = 10000

def create_grid(mynumber):
    grid_dic = {}
    for index in range(-mynumber, (mynumber + 1)):
        rowlist = []
        for number in range(mynumber * 2 + 1):
            rowlist.append(".")
        grid_dic[index] = rowlist
    middle = int(len(rowlist) / 2)
    grid_dic[0][middle] = "o"
    return grid_dic

grid_dic = create_grid(grid_size)

def print_grid(grid_dic):
    variability = int(len(grid_dic) / 2)
    for every in range(variability, -variability -1, -1):
        print(every, grid_dic[every])

##print_grid(grid_dic)

def get_wirelist(myfile):
    wire = myfile.readline()
    wire = wire.rstrip("\n")
    wirelist = wire.split(",")
##    print(wirelist)
    return wirelist

wirelist = get_wirelist(wires)
    
def upload_wire(mylist):
    row = 0
    column = int(len(grid_dic) / 2)
    for instruction in mylist:
        direction = instruction[0]
        amount = int(instruction[1:])
        if direction == "U":
            for unit in range(1, amount + 1):
                grid_dic[row + unit][column] = "W"
            row += amount
        elif direction == "R":
            for unit in range(1, amount + 1):
                grid_dic[row][column + unit] = "W"
            column += amount
        elif direction == "D":
            for unit in range(1, amount + 1):
                grid_dic[row - unit][column] = "W"
            row -= amount
        elif direction == "L":
            for unit in range(1, amount + 1):
                grid_dic[row][column - unit] = "W"
            column -= amount
    print("Wire uploaded.")

upload_wire(wirelist)
##print_grid(grid_dic)
wirelist = get_wirelist(wires)

def upload_intersections(mylist):
    intersections = [grid_size * 2]
    row = 0
    column = int(len(grid_dic) / 2)
    middle = int(len(grid_dic) / 2)
    for instruction in mylist:
        direction = instruction[0]
        amount = int(instruction[1:])
        if direction == "U":
            for unit in range(1, amount + 1):
                if abs(row + unit) + abs(column - middle) > min(intersections):
                    None
                elif grid_dic[row + unit][column] == "W":
##                    grid_dic[row + unit][column] = "X"
                    manhattan = abs(row + unit) + abs(column - middle)
                    intersections.append(manhattan)
##                else:
##                    grid_dic[row + unit][column] = "W"
            row += amount
        elif direction == "R":
            for unit in range(1, amount + 1):
                if abs(row) + abs(column + unit - middle) > min(intersections):
                    None
                elif grid_dic[row][column + unit] == "W":
##                    grid_dic[row][column + unit] = "X"
                    manhattan = abs(row) + abs(column + unit - middle)
                    intersections.append(manhattan)
##                else:
##                    grid_dic[row][column + unit] = "W"
            column += amount
        elif direction == "D":
            for unit in range(1, amount + 1):
                if abs(row - unit) + abs(column - middle) > min(intersections):
                    None
                elif grid_dic[row - unit][column] == "W":
##                    grid_dic[row - unit][column] = "X"
                    manhattan = abs(row - unit) + abs(column - middle)
                    intersections.append(manhattan)
##                else:
##                    grid_dic[row - unit][column] = "W"
            row -= amount
        elif direction == "L":
            for unit in range(1, amount + 1):
                if abs(row) + abs(column - unit - middle) > min(intersections):
                    None
                elif grid_dic[row][column - unit] == "W":
##                    grid_dic[row][column - unit] = "X"
                    manhattan = abs(row) + abs(column - unit - middle)
                    intersections.append(manhattan)
##                else:
##                    grid_dic[row][column - unit] = "W"
            column -= amount
    print("Behold a list of intersections!\n", intersections)
    print("The smallest Manhattan distance, and thus the result, is:", min(intersections))

upload_intersections(wirelist)
##print_grid(grid_dic)

