wires = open(r"C:\Python34\advent-of-code-2019\Day3 - Wires\wires.txt", "r")
grid_size = 10000

def create_grid(mynumber):
    grid_dic = {}
    for index in range(-mynumber, (mynumber + 1)):
        rowlist = []
        for number in range(mynumber * 2 + 1):
            rowlist.append(".")
        grid_dic[index] = rowlist
    middle = int(len(rowlist) / 2)
    grid_dic[0][middle] = "o"
    return grid_dic

grid_dic = create_grid(grid_size)

def print_grid(grid_dic):
    variability = int(len(grid_dic) / 2)
    for every in range(variability, -variability -1, -1):
        print(every, grid_dic[every])

##print_grid(grid_dic)

def get_wirelist(myfile):
    wire = myfile.readline()
    wire = wire.rstrip("\n")
    wirelist = wire.split(",")
##    print(wirelist)
    return wirelist

wirelist1 = get_wirelist(wires)
    
def upload_wire(mylist):
    row = 0
    column = int(len(grid_dic) / 2)
    for instruction in mylist:
        direction = instruction[0]
        amount = int(instruction[1:])
        if direction == "U":
            for unit in range(1, amount + 1):
                grid_dic[row + unit][column] = "W"
            row += amount
        elif direction == "R":
            for unit in range(1, amount + 1):
                grid_dic[row][column + unit] = "W"
            column += amount
        elif direction == "D":
            for unit in range(1, amount + 1):
                grid_dic[row - unit][column] = "W"
            row -= amount
        elif direction == "L":
            for unit in range(1, amount + 1):
                grid_dic[row][column - unit] = "W"
            column -= amount
    print("Wire uploaded.")

upload_wire(wirelist1)
##print_grid(grid_dic)
wirelist2 = get_wirelist(wires)

def upload_intersections(mylist):
    row = 0
    column = int(len(grid_dic) / 2)
    middle = int(len(grid_dic) / 2)
    steps = 0
    intersections = {}
    for instruction in mylist:
        direction = instruction[0]
        amount = int(instruction[1:])
        if direction == "U":
            for unit in range(1, amount + 1):
                if abs(row + unit) > grid_size or abs(column) > grid_size:
                    None
                elif grid_dic[row + unit][column] == "W":
                    grid_dic[row + unit][column] = "X"
                    actual_steps = steps + unit
                    manhattan = abs(row + unit) + abs(column - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row + unit][column] = "W"
            row += amount
            steps += amount
        elif direction == "R":
            for unit in range(1, amount + 1):
                if abs(row) > grid_size or abs(column + unit) > grid_size:
                    None
                elif grid_dic[row][column + unit] == "W":
                    grid_dic[row][column + unit] = "X"
                    actual_steps = steps + unit
                    manhattan = abs(row) + abs(column + unit - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row][column + unit] = "W"
            column += amount
            steps += amount
        elif direction == "D":
            for unit in range(1, amount + 1):
                if abs(row - unit) > grid_size or abs(column) > grid_size:
                    None
                elif grid_dic[row - unit][column] == "W":
                    grid_dic[row - unit][column] = "X"
                    actual_steps = steps + unit
                    manhattan = abs(row - unit) + abs(column - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row - unit][column] = "W"
            row -= amount
            steps += amount
        elif direction == "L":
            for unit in range(1, amount + 1):
                if abs(row) > grid_size or abs(column - unit) > grid_size:
                    None
                elif grid_dic[row][column - unit] == "W":
                    grid_dic[row][column - unit] = "X"
                    actual_steps = steps + unit
                    manhattan = abs(row) + abs(column - unit - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row][column - unit] = "W"
            column -= amount
            steps += amount
    print("Intersections uploaded.")
    print(intersections)
    return intersections

intersection_dic1 = upload_intersections(wirelist2)
##print_grid(grid_dic)

def pair_intersections(mylist):
    row = 0
    column = int(len(grid_dic) / 2)
    middle = int(len(grid_dic) / 2)
    steps = 0
    intersections = {}
    for instruction in mylist:
        direction = instruction[0]
        amount = int(instruction[1:])
        if direction == "U":
            for unit in range(1, amount + 1):
                if grid_dic[row + unit][column] == "X":
                    actual_steps = steps + unit
                    manhattan = abs(row + unit) + abs(column - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row + unit][column] = "W"
            row += amount
            steps += amount
        elif direction == "R":
            for unit in range(1, amount + 1):
                if grid_dic[row][column + unit] == "X":
                    actual_steps = steps + unit
                    manhattan = abs(row) + abs(column + unit - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row][column + unit] = "W"
            column += amount
            steps += amount
        elif direction == "D":
            for unit in range(1, amount + 1):
                if grid_dic[row - unit][column] == "X":
                    actual_steps = steps + unit
                    manhattan = abs(row - unit) + abs(column - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row - unit][column] = "W"
            row -= amount
            steps += amount
        elif direction == "L":
            for unit in range(1, amount + 1):
                if grid_dic[row][column - unit] == "X":
                    actual_steps = steps + unit
                    manhattan = abs(row) + abs(column - unit - middle)
                    intersections[manhattan] = actual_steps
##                else:
##                    grid_dic[row][column - unit] = "W"
            column -= amount
            steps += amount
    print("Intersections paired.")
    print(intersections)
    return intersections

intersection_dic2 = pair_intersections(wirelist1)

def get_stepamounts(mydic1, mydic2):
    steps = []
    for key in mydic1:
        value = mydic1[key] + mydic2[key]
        steps.append(value)
    print("Behold the required steps!", steps)
    print("The lowest one is", min(steps))

get_stepamounts(intersection_dic1, intersection_dic2)
